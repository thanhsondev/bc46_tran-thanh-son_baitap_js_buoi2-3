// Bài 1: Tính tiền lương nhân viên
function tongLuong() {
    var tienLuong1Ngay = document.getElementById('luong1Ngay').value;
    var ngayLam = document.getElementById('soNgayLam').value;
    var tongLuong = 0;
    tongLuong = ngayLam * tienLuong1Ngay;
    
    document.getElementById('ketQua').innerHTML = tongLuong.toLocaleString() + 'VND';
 
 }
 
 //Bài 2: Tính giá trị trung bình
 function tinhTrungBinh() {
     var thu1 = document.getElementById('soThu1').value * 1;
     var thu2 = document.getElementById('soThu2').value * 1;
     var thu3 = document.getElementById('soThu3').value * 1;
     var thu4 = document.getElementById('soThu4').value * 1;
     var thu5 = document.getElementById('soThu5').value * 1;
 
     var sotrungbinh = 0
 
     sotrungbinh = (thu1 + thu2 + thu3 + thu4 + thu5) / 5;
 
     document.getElementById('ketQuaTB').innerHTML = sotrungbinh;
 
 }
 
 //Bài 3: Quy đổi tiền
 function quyDoi() {
     var nhapUSD = document.getElementById('tienUSD').value;
     var ketQuaQD = 0;
     ketQuaQD = nhapUSD * 23500;
     document.getElementById('ketQuaQD').innerHTML = ketQuaQD.toLocaleString() + "VND";
 }
 
 //Bài 4: Tính diện tích, chu vi hình chủ nhật
 function tinh() {
     var chieuDai = document.getElementById('chieuDai').value*1;
     var chieuRong = document.getElementById('chieuRong').value*1;
     var dienTich = 0;
     var chuVi = 0;
     dienTich = chieuDai * chieuRong;
     chuVi = (chieuDai + chieuRong ) * 2;
     document.getElementById('tinhKQ').innerHTML =`Diện tích: ${dienTich} - Chu vi: ${chuVi} `;
 }
 
 //Bài 5: Tính tổng 2 ký số
 function ketQuaKS() {
  var numBer = document.getElementById('numBer').value;
  var soDonVi = Math.floor(numBer%10);
  var soHangChuc = Math.floor(numBer/10);
  numBer = soDonVi + soHangChuc;
  document.getElementById('showKetQua').innerHTML = `<h2 class ="text-light"> ${numBer} </h2>`;
 }